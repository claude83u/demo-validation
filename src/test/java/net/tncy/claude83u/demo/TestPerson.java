package net.tncy.claude83u.demo;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

public class TestPerson {
	
	private static Validator validator;
	
	@BeforeClass
	public static void setUpClass() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void validateWellDefinedPerson() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(1995, Calendar.JULY, 26);
		Person p = new Person("Romain", "Marlier", calendar.getTime(), "Fr");
		Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
		assertEquals(0, constraintViolations.size());
	}
	
	@Test
	public void validateAdult() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(1974, Calendar.DECEMBER, 3);
		Date birthDate = calendar.getTime();
		Person p = new Person("Romain", "Marlier", birthDate, "FR");
		Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p, AdultCheck.class);
		assertEquals(0, constraintViolations.size());
	}


}
