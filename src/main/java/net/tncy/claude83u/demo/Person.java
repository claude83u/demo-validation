package net.tncy.claude83u.demo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Calendar;
import java.util.Date;
import javax.validation.constraints.Min;

public class Person {

	@NotNull
	@Size(min=1)
	private String firstName;
	
	@NotNull
	@Size(min=1)
	private String lastName;
	
	@NotNull(groups = {AdultCheck.class})
	private Date birthDate;
	private String cityzenship;
	
	@NotNull(groups = {AdultCheck.class})
	@Min(value=18, groups = {AdultCheck.class})
	private transient Integer age;
	
	public Person(String firstName, String lastName, Date birthDate, String cityzenship) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.cityzenship = cityzenship;
		
		computeAge();
	}
	
	private void computeAge() {
		if(birthDate != null) {
			Calendar curr = Calendar.getInstance();
			Calendar birth = Calendar.getInstance();
			birth.setTime(birthDate);
			this.age = curr.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
		} else {
			age = null;
		}
	}
}
